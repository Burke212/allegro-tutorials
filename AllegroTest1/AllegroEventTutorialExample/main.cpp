/*
Code goes over events and timers.
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stdio.h>

const float FPS = 60; // Declare screen FPS. See timer object definition.

int main()
{
	ALLEGRO_DISPLAY *display = NULL; // Create display variable.
	ALLEGRO_EVENT_QUEUE *event_queue = NULL; // Create event queue variable.
	ALLEGRO_TIMER *timer = NULL; // Create timer variable.
	bool redraw = true; // ???

	al_init();// Call Allegro to initialize. 

	// Error handling if al_init()/ Allegro library doesn't initialize.
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	al_init_font_addon();

	// Create timer object w/ given value.
	timer = al_create_timer(1.0 / FPS);

	// Error handling for timer.
	if (!timer) {
		fprintf(stderr, "failed to create timer!\n");
		return -1;
	}

	// Create display object w/ given parameters.
	display = al_create_display(640, 480);
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		al_destroy_timer(timer);
		return -1;
	}

	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "failed to create event_queue!\n");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}


	// Error handling if display fails.	
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}

	ALLEGRO_FONT* font = al_create_builtin_font();

	// clears the current display to a given color.
	al_clear_to_color(al_map_rgb(0, 0, 0));

	// Draws given text w/ given color & @ given location on screen. 
	al_draw_text(font, al_map_rgb(255, 255, 255), 400, 300, ALLEGRO_ALIGN_CENTER, "Welcome to Allegro!");

	// Calls the swap chain. 
	al_flip_display();

	// Attempts to cause the program to wait for the given amount of time. 
	al_rest(5.0);


	// Destroys our display and frees the memory and should be called when you are about to shut down the program.
	al_destroy_display(display);


	return 0;
}