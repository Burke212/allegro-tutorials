/*
	Code goes over basic window display.
	Also covers how to determine user's monitor resolution  & display window in min/ max resolution. 
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stdio.h>

int main()
{
	al_init();

	// Error handling if al_init()/ Allegro library doesn't initialize.
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	al_init_font_addon();

	// Code to display a window given the size parameters.
	ALLEGRO_DISPLAY* display = al_create_display(800, 600);


	/* FOLLOWING CODE HANDLES MIN/ MAX SCREEN RESOLUTION. 
	// Display a window w/o defining it's size. 
	ALLEGRO_DISPLAY* display = NULL;
	*/

	/* FOLLOWING CODE HANDLES MIN/ MAX SCREEN RESOLUTION. 
	// Holds monitor information. Use to make the display window fit the screen. 
	ALLEGRO_DISPLAY_MODE   disp_data;

	// Function gets monitor's min resolution, & stores this in "&disp_data".
	al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);// First parameter gets last index of monitor's resolution.
																	// To get monitor's max res., change "al_get_num_display_modes()"
																	// to 0.

	al_set_new_display_flags(ALLEGRO_FULLSCREEN);// Tells Allegro to display in fullscreen mode.
	display = al_create_display(disp_data.width, disp_data.height);// Create display with the minimum resolution available.
	*/


	// Error handling if display fails.	
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}

	ALLEGRO_FONT* font = al_create_builtin_font();

	// clears the current display to a given color.
	al_clear_to_color(al_map_rgb(0, 0, 0));

	// Draws given text w/ given color & @ given location on screen. 
	al_draw_text(font, al_map_rgb(255, 255, 255), 400, 300, ALLEGRO_ALIGN_CENTER, "Welcome to Allegro!");
	
	// Calls the swap chain. 
	al_flip_display();

	// Attempts to cause the program to wait for the given amount of time. 
	al_rest(5.0);


	// Destroys our display and frees the memory and should be called when you are about to shut down the program.
	al_destroy_display(display);


	return 0;
}